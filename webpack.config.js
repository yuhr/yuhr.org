'use strict';

const webpack = require('webpack');
const IS_PROD = process.env.NODE_ENV === 'production';
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const path = require('path');
const fs = require('fs');
const { JSDOM } = require('jsdom');

const process_html = (content, srcpath) => {
  const dom = new JSDOM(content, { runScripts: "outside-only" });

  Array.prototype.forEach.call(dom.window.document.querySelectorAll('meta[name=include]'), function (include) {
    const include_name = path.dirname(srcpath) + '/' + include.getAttribute('content');
    const include_file = fs.readFileSync(include_name, 'utf8');
    const include_dom = new JSDOM('<body><div>' + include_file + '</div></body>');
    Array.prototype.forEach.call(include_dom.window.document.querySelectorAll('body>div>*'), function (child) {
      include.parentNode.insertBefore(child, include);
    });
    include.parentNode.removeChild(include);
  });

  Array.prototype.forEach.call(dom.window.document.querySelectorAll('body script.--baked'), function (script) {
    const result = dom.window.eval(script.textContent);
    if ('string' == typeof result) {
      const result_dom = new JSDOM('<body><div>' + result + '</div></body>');
      Array.prototype.forEach.call(result_dom.window.document.querySelector('body > div').childNodes, function (child) {
        script.parentNode.insertBefore(child, script);
      });
    }
    script.parentNode.removeChild(script);
  });

  return dom.serialize();
}

module.exports = [{
  context: __dirname + '/src',
  entry: {
    bundle: "./bundle.js",
    index: "./index.js"
  },
  output: {
    path: __dirname + '/dst',
    filename: './[name].js'
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, use: { loader: 'babel-loader', options: { presets: ['@babel/preset-env'] } } },
      { test: /\.s?css$/, use: ExtractTextPlugin.extract({ use: [
        { loader: 'css-loader', options: { importLoaders: 1 } },
        { loader: 'postcss-loader', options: {
          plugins: [
            require('postcss-import')({ addDependencyTo: 'webpack' }),
            require('postcss-nesting')(),
            require('postcss-cssnext')({ browsers: ['last 2 versions'] }),
            require('perfectionist')({ indentSize: 2 })
          ],
          publicPath: '../'
        } }] }) },
      { test: /\.(?:woff2|jpg|xml|txt|asc)$/, use: { loader: 'file-loader', options: { name: '[path][name].[ext]' } } },
      { test: /\.htaccess$/, use: { loader: 'file-loader', options: { name: '[path][name]' } } },
      { test: /\.html$/, use: [
        { loader: 'file-loader', options: { name: '[path][name].[ext]' } },
        'extract-loader',
        { loader: 'html-loader', options: { conservativeCollapse: false } },
        { loader: 'process-loader', options: { process: process_html } }
        ] }
    ]
  },
  plugins: [
    new ExtractTextPlugin('[name].css')
  ]
},
{
  context: __dirname + '/src/qr',
  entry: {
    bundle: "./bundle.js",
    index: "./index.js"
  },
  output: {
    path: __dirname + '/dst/qr',
    filename: './[name].js'
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, use: { loader: 'babel-loader', options: { presets: ['@babel/preset-env'] } } },
      { test: /\.s?css$/, use: ExtractTextPlugin.extract({ use: [
        { loader: 'css-loader', options: { importLoaders: 1 } },
        { loader: 'postcss-loader', options: {
          plugins: [
            require('postcss-import')({ addDependencyTo: 'webpack' }),
            require('postcss-nesting')(),
            require('postcss-cssnext')({ browsers: ['last 2 versions'] }),
            require('perfectionist')({ indentSize: 2 })
          ],
          publicPath: '../'
        } }] }) },
      { test: /\.(?:woff2|jpg|png|svg)$/, use: { loader: 'file-loader', options: { name: '[path][name].[ext]' } } },
      { test: /\.html$/, use: [
        { loader: 'file-loader', options: { name: '[path][name].[ext]' } },
        'extract-loader',
        { loader: 'html-loader', options: { conservativeCollapse: false } },
        { loader: 'process-loader', options: { process: process_html } }
        ] }
    ]
  },
  plugins: [
    new ExtractTextPlugin('[name].css')
  ]
}];

if (IS_PROD) {
  module.exports.forEach((options) => {
    options.plugins.concat([
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          drop_console: false,
          unsafe: true,
          hoist_vars: true,
          collapse_vars: true,
          pure_getters: true
        }
      })
    ]);
  });
}