'use strict';

import './index.css';
import './index.html';
import './pubkey.asc';
import './robots.txt';
import './sitemap.xml';
import './.well-known/keybase.txt';